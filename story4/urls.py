"""story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from story4_app import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home , name='home'),
    path('resume/',views.resume , name='resume'),
    path('story1/',views.story1 , name='story1'),
    path('story5/add',views.story5_add, name='story5_add'),
    path('story5/',views.story5, name='story5'),
    path('story5/<int:my_id>/', views.story5_detail, name='story5_detail'),
    path('story5/<int:my_id>/delete', views.delete_view, name='matkul_delete'),
    path('story6/',include('story6.urls')),
    path('story7/', include('story7.urls')),
    path('story8/', include('story_8.urls')),
    path('story9/', include('story_9.urls')),
]
