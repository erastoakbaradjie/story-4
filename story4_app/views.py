from django.shortcuts import render, redirect

from django.shortcuts import get_object_or_404, render, HttpResponseRedirect 

from .forms import ProductForm

from .models import Matkul

# Create your views here.
def home(request):
    return render(request,'main.html')

def resume(request):
    return render(request,'index2.html')

def story1(request):
    return render(request,'index.html')

def story5(request):
    obj = Matkul.objects.all()
    context = {
        'object': obj
    }
    return render(request, "matkul_list.html", context)

def story5_add(request):
    form = ProductForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('home')

    context = {
        'form': form
    }
    return render(request, "matkul_create.html", context)

def story5_detail(request, my_id):
    obj = Matkul.objects.get(id=my_id)
    context = {
        'object':obj
    }
    return render(request, "matkul_detail.html", context)

def delete_view(request, my_id): 
    obj = Matkul.objects.get(id=my_id)
    obj.delete()  
    return redirect ('story5') 