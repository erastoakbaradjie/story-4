from django.db import models

class Matkul(models.Model) :
    title = models.CharField(max_length=100)
    description = models.TextField()
    dosen = models.CharField(max_length=100)
    sks = models.PositiveSmallIntegerField()
    semester = models.PositiveIntegerField()
    room = models.CharField(max_length=10)

    def __str__(self): 
        return self.title

    #Title (str), Description (str), dosen (str), sks (int), smester tahun (int), ruang kelas (str)
# Create your models here.
