from django import forms

from .models import Matkul

class ProductForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'title',
            'description',
            'dosen',
            'sks',
            'semester',
            'room'
        ]