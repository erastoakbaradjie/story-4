from django.shortcuts import render
from django.http.response import JsonResponse
import urllib.request, json


# Create your views here.
def home(request):
    return render(request, 'search.html')

def search_book(request,title):
    link = urllib.parse.quote("https://www.googleapis.com/books/v1/volumes?q="+title,"/:?=")
    with urllib.request.urlopen(link) as url:
        global data
        data = json.loads(url.read().decode())
    return JsonResponse(data)
