from django.test import TestCase, Client
from django.urls import resolve
from .views import search_book, home

# Create your tests here.
class Story8Test(TestCase):
    # Checking URLS
    def test_url_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)

    def test_api_exist(self):
        response = Client().get('/story8/Genshin%20Impact/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_views_exist(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, home)

    def test_api_views_exist(self):
        found = resolve('/story8/Genshin%20Impact/')
        self.assertEqual(found.func, search_book)

    # Checking Templates
    def test_templates_exist(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response,'search.html')
