from django.contrib import auth
from django.contrib.auth.models import User
from django.http import response
from django.test import TestCase , Client
from django.urls import resolve
from .views import register

# Create your tests here.
class Story9Test(TestCase):

    # Checking URLS
    def test_login_url_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code,200)

    def test_logout_url_exist(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code,200)

    def test_register_url_exist(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_register_views_exist(self):
        found = resolve('/story9/register/')
        self.assertEqual(found.func, register)

    # Checking Templates
    def test_login_templates_exist(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'auth.html')

    def test_logout_templates_exist(self):
        response = Client().get('/story9/logout/')
        self.assertTemplateUsed(response, 'logout.html')

    def test_register_activities_templates_exist(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'register.html')

    # Checking register,login
    def test_register(self):
        response = self.client.post('/story9/register/', {'first_name': 'first', 'last_name': 'last', 'email' : 'asd@gmail.com','username':'hehehe', 'password1' : 'agfeoqwegnhoqwgbqn', 'password2' : 'agfeoqwegnhoqwgbqn'},format='text/html')
        amount = User.objects.count()
        self.assertEqual(amount,1)

