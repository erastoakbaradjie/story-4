from django.test import TestCase , Client
from django.urls import resolve
from . import models
from .models import People, Events
from .views import add, display, add_events
# Create your tests here.

class Story6Test(TestCase):
    #SETTING UP THE DATABASE
    def setUp(self):
        self.events = Events.objects.create(title="Riding Trains")
        People.objects.create(full_name="Rengoku Kyojuro", nickname="Kyojuro", events_registered = self.events)

    #CHECKING DATABASE
    def test_check_amount_people_database(self):
        self.assertEqual(len(People.objects.all()), 1)

    def test_check_amount_events_database(self):
        self.assertEqual(len(Events.objects.all()), 1)

    #CHECK URL
    def test_add_url_exist(self):
        response = Client().get('/story6/add/')
        self.assertEqual(response.status_code,200)

    def test_display_url_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code,200)

    def test_add_events_url_exist(self):
        response = Client().get('/story6/add_events/')
        self.assertEqual(response.status_code,200)
    
    #CHECK VIEWS
    def test_add_views_exist(self):
        found = resolve('/story6/add/')
        self.assertEqual(found.func, add)

    def test_display_views_exist(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, display)

    def test_add_events_views_exist(self):
        found = resolve('/story6/add_events/')
        self.assertEqual(found.func, add_events)

    # CHECK HTML

    def test_if_HTML_display_models(self):
        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn("Riding Trains", html_response)
        self.assertIn("Rengoku Kyojuro", html_response)

     # CHECKING TEMPLATES
    def test_add_templates_exist(self):
        response = Client().get('/story6/add/')
        self.assertTemplateUsed(response, 'add_people.html')

    def test_display_templates_exist(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'main_page.html')

    def test_add_events_templates_exist(self):
        response = Client().get('/story6/add_events/')
        self.assertTemplateUsed(response, 'add_event.html')

     # Checking Form Exist & works
    def test_saving_a_POST_activities_request(self):
        response = Client().post('/story6/add_events/', data={'title': 'killing demons'})
        amount = models.Events.objects.filter(title="killing demons").count()
        self.assertEqual(amount, 1)

    def test_saving_a_POST_request(self):
        events2 = Events.objects.get(pk=1).pk
        response = Client().post('/story6/add/', data={'full_name': 'Kyojuro Rengoku', 'nickname': 'Kyojuro', 'events_registered' : events2})
        amount = models.People.objects.filter(full_name='Kyojuro Rengoku').count()
        self.assertEqual(amount, 1)