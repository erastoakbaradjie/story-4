from django.db import models

class Events(models.Model) :
    title = models.CharField(max_length=100)
    
    def __str__(self):
        return self.title

class People(models.Model):
    full_name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=20)
    events_registered = models.ForeignKey(Events, on_delete=models.CASCADE)

    def __str__(self):
        return self.full_name