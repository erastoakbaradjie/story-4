from django.db.models import fields
from django import forms
from django.forms import ModelForm, Textarea, TextInput
from django.forms import widgets
from .models import People, Events

# Form Class
class PeopleForm(ModelForm):
    class Meta:
        model = People
        fields = ['full_name','nickname','events_registered']
        widgets = {
            'full_name': TextInput(attrs={'placeholder': 'ex: Rengoku Kyojuro', 'required': True}),
            'nickname': TextInput(attrs={'placeholder': 'ex: Kyojuro', 'required': True}),
            }

class EventsForm(ModelForm):
    class Meta:
        model = Events
        fields = ['title']
        widgets = {
            'title': TextInput(attrs={'placeholder': 'ex: Riding Trains', 'required': True}),
        }
