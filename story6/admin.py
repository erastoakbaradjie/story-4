from django.contrib import admin
from .models import Events,People

# Register your models here.
admin.site.register(Events)
admin.site.register(People)
