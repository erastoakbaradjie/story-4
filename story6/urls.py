from django.urls import path
from .views import add, display, add_events
#url for app
urlpatterns = [
    path('', display, name='main_page'),
    path('add/', add, name='add_people'),
    path('add_events/', add_events, name='add_event')
]
