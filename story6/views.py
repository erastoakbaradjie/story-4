from django.shortcuts import redirect,render
from django.http import request
from .models import Events, People
from .forms import EventsForm, PeopleForm

# Create your views here.

def add(request):
    form = PeopleForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('main_page')
    context = {'page_title':'Register','form':form}
    return render(request, 'add_people.html', context)

def display(request):
    people = People.objects.all()
    events = Events.objects.all()
    context = {'people': people, 'events': events, 'page_title' : "Events"}
    return render(request, "main_page.html",context)

def add_events(request):
    form = EventsForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('main_page')
    context = {'page_title': 'Register', 'form':form}
    return render(request, 'add_event.html', context)
